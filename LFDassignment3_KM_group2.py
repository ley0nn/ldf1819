# Author: Leon Graumans, Mike Zhang
# s2548798, s2988240
# 02-10-2018 | Revision: 1
# Course Lecturer: dr. M. Nissim


from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.cluster import KMeans
from sklearn.metrics import adjusted_rand_score, homogeneity_completeness_v_measure, classification_report

import numpy as np
import argparse
import string
import sys

from nltk.tokenize import word_tokenize
from nltk.stem.snowball import SnowballStemmer
from nltk.corpus import stopwords

np.random.seed(1337)

# command: python3 LFDassignment3_SVM_group2.py trainset.txt [testset.txt] [-2] [-sent]

def remove_stopwords_punc(tokens):

    stopwords_list = stopwords.words("english")
    whitelist = ['why', 'not', 'no', 'against', '!', '?', 'good', 'yes']
    tokens = [word for word in tokens if (word not in stopwords_list or word in whitelist)]
    tokens = [word for word in tokens if (word not in string.punctuation or word in whitelist)]

    return tokens

def read_corpus(corpus_file, k, s):

    documents = []
    labels = []
    ss = SnowballStemmer("english")
    with open(corpus_file, encoding='utf-8') as f:
        for line in f:
            tokens = word_tokenize(line)

            if k == 2:

                ###model for K=2 sentiment###
                if s == True:
                    if tokens[0] == 'music' or tokens[0] == 'health':  # for two-class 'music' and 'health' and their sentiment
                        labels.append(tokens[1]) #for sentiment
                        documents.append(' '.join(tokens[3:]))
                    else:
                        continue

               ###model for K=2 topics###
                else:
                    if tokens[0] == 'music' or tokens[0] == 'health':  # for two-class 'music' and 'health'
                        tokens = remove_stopwords_punc(tokens)
                        labels.append(tokens[0]) #for topic
                        tokens = [ss.stem(token) for token in tokens]
                        documents.append(' '.join(tokens[3:]))
                    else:
                        continue

            ###model for K=6 topics###
            else:
                tokens = remove_stopwords_punc(tokens)
                labels.append(tokens[0])  # for six-class
                tokens = [ss.stem(token) for token in tokens]
                documents.append(' '.join(tokens[3:]))


    return np.array(documents), np.array(labels)


def kmeans(xtrain,ytrain,xtest,ytest, k):


    vectorizer = TfidfVectorizer(ngram_range=(1,2))

    xtrain = vectorizer.fit_transform(xtrain, ytrain)
    xtest = vectorizer.transform(xtest)

    km = KMeans(n_clusters=k, n_init=10, verbose=0, random_state=500)

    km.fit(xtrain,ytrain)
    yguess = km.predict(xtest)
    ri = adjusted_rand_score(ytest, yguess)
    vm = homogeneity_completeness_v_measure(ytest, yguess)
    print(km.labels_)

    return ri, vm

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('trainset', help='required')
    parser.add_argument('testset', nargs='?', default='', help='optional')
    parser.add_argument('-2', '--twoClustering', action='store_true',
                        help='if -2 is given, will do two class clustering for topics, '
                             'if -sent is also given, will do sentiment')
    parser.add_argument('-sent', '--sentClustering', action='store_true',
                        help='if -sent is given, should do it together with -2')
    args = parser.parse_args()


    if args.twoClustering and args.sentClustering:
        print('### doing sentiment clustering... ###')
        s = True
        k = 2

    elif args.twoClustering:
        print('### doing topic clustering (2 classes)... ###')
        s = False
        k = 2

    elif args.sentClustering:
        parser.print_help()
        sys.exit()

    else:
        print('### doing topic clustering (6 classes)... ###')
        s = False
        k = 6

    if args.testset:
        print('### found testset, using it... ###')
        xtrain, ytrain = read_corpus(args.trainset, k, s)
        xtest, ytest = read_corpus(args.testset, k, s)

    else:
        print('### use trainset for training and testing... ###')
        X, Y = read_corpus(args.trainset, k, s)
        split_point = int(0.75 * len(X))
        xtrain = X[:split_point]
        ytrain = Y[:split_point]
        xtest = X[split_point:]
        ytest = Y[split_point:]

    ri, vm = kmeans(xtrain, ytrain, xtest, ytest, k)
    print('\n## Rand Index, Homogeneity, Completeness, V-measure:')
    print('Rand Index: {:.3}'.format(ri))
    print('Homogeneity: {:.3}'.format(vm[0]))
    print('Completeness: {:.3}'.format(vm[1]))
    print('V-measure: {:.3}'.format(vm[2]))
