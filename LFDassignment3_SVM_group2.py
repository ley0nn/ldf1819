# Author: Leon Graumans, Mike Zhang
# s2548798, s2988240
# 02-10-2018 | Revision: 1
# Course Lecturer: dr. M. Nissim


from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score, f1_score, classification_report
from sklearn.model_selection import GridSearchCV

import numpy as np
import argparse

from nltk.tokenize import word_tokenize
from nltk.stem import PorterStemmer

np.random.seed(1337)

# command: python3 LFDassignment3_SVM_group2.py trainset.txt [testset.txt] [-gs]

def read_corpus(corpus_file):

    documents = []
    labels = []
    ps = PorterStemmer()
    with open(corpus_file, encoding='utf-8') as f:
        for line in f:
            tokens = word_tokenize(line)
            labels.append(tokens[1])
            tokens = [ps.stem(token) for token in tokens]
            documents.append(' '.join(tokens[3:]))


    return np.array(documents), np.array(labels)

def svm(xtrain,ytrain,xtest,ytest):


    classifier = Pipeline([
        ('features', FeatureUnion([
            ('tfidf', TfidfVectorizer(ngram_range=(1,2)))
        ])),
            ('cls', SVC(kernel='rbf', gamma=0.7, C=5))
        ])


    classifier.fit(xtrain, ytrain)
    yguess = classifier.predict(xtest)
    print(classification_report(ytest,yguess))
    acc = accuracy_score(ytest, yguess)
    f1 = f1_score(ytest, yguess, average="weighted")

    return acc, f1


def gridsearch(xtrain, ytrain):

    tfidf = TfidfVectorizer()
    classifier = Pipeline([
        ('features', FeatureUnion([
            ('tfidf', tfidf),
        ])),
            ('cls', SVC(kernel='rbf',gamma=0.7, C=1.0))
    ])

    parameters = {
        # 'cls__C': [3.0, 4.0, 5.0],
        # 'cls__gamma': [0.2, 0.4, 0.7, 1.0],
    }

    grid = GridSearchCV(classifier, param_grid=parameters, cv=2)
    grid.fit(xtrain, ytrain)
    print(grid.best_params_)
    print('best_score: {:.3}'.format(grid.best_score_))
    exit()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('trainset', help='required')
    parser.add_argument('testset', nargs='?', default='', help='if testset is present, gridsearch is not available.')
    parser.add_argument('-gs', '--gridsearch', action='store_true', help='use a gridsearch.')
    args= parser.parse_args()

    if args.gridsearch:
        print('### running gridsearch... ###')
        xtrain, ytrain = read_corpus(args.trainset)
        gridsearch(xtrain,ytrain)

    elif args.testset:
        print('### found testset, using it... ###')
        xtrain, ytrain = read_corpus(args.trainset)
        xtest, ytest = read_corpus(args.testset)

    else:
        print('### use trainset for training and testing... ###')
        X, Y = read_corpus(args.trainset)
        split_point = int(0.75 * len(X))
        xtrain = X[:split_point]
        ytrain = Y[:split_point]
        xtest = X[split_point:]
        ytest = Y[split_point:]

    acc, f1 = svm(xtrain,ytrain,xtest,ytest)
    print('\n## accuracy and f1_score:')
    print('accuracy: {:.3}'.format(acc))
    print('f1_score: {:.3}'.format(f1))